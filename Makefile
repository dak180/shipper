# Makefile for the shipper project

VERS=$(shell sed <shipper -n -e '/^shipper_version *= *\(.*\)/s//\1/p')

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

DOCS    = README TODO COPYING shipper.xml
SOURCES = shipper Makefile $(DOCS) control shipper-logo.png

all: shipper-$(VERS).tar.gz

install: shipper.1
	install -d "$(target)/bin"
	install -m 755 shipper "$(target)/bin/"
	install -d "$(target)/$(mandir)/man1"
	gzip <shipper.1 >"$(target)/$(mandir)/man1/shipper.1.gz"

shipper.1: shipper.xml
	xmlto man shipper.xml
shipper.html: shipper.xml
	xmlto html-nochunks shipper.xml

EXTRA = shipper.1
shipper-$(VERS).tar.gz: $(SOURCES) $(EXTRA)
	@mkdir shipper-$(VERS)
	@cp $(SOURCES) $(EXTRA) shipper-$(VERS)
	@tar -czf shipper-$(VERS).tar.gz shipper-$(VERS)
	@rm -fr shipper-$(VERS)

shipper-$(VERS).md5: shipper-$(VERS).tar.gz
	@md5sum shipper-$(VERS).tar.gz >shipper-$(VERS).md5

clean:
	rm -f *.1 *.tar.gz *.rpm *.tar.gz *.html *.md5 *.sha*

check: pylint
	cd test; $(MAKE) --quiet

version:
	echo $(VERS)

PYLINTOPTS = --rcfile=/dev/null --reports=n  --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" --dummy-variables-rgx='^_' --disable=C0103,C0111,C0301,C0302,C0325,C0326,C0330,C1001,W0122,W0110,W0141,W0511,W0123,W0603,W0612,W0621,W0631,R0902,R0903,R0912,R0915
pylint:
	@pylint $(PYLINTOPTS) shipper

dist: shipper-$(VERS).tar.gz shipper-$(VERS).md5 

release: check shipper-$(VERS).tar.gz shipper-$(VERS).md5 shipper.html
	shipper version=$(VERS) | sh -x -e

refresh: shipper-$(VERS).md5 shipper.html
	shipper -N -w version=$(VERS) | sh -x -e

